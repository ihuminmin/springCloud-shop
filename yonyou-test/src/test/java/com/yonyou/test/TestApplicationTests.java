package com.yonyou.test;

import com.google.gson.Gson;
import com.yonyou.test.api.ApiResult;
import com.yonyou.test.util.SignHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplicationTests {
    //有效期2小时
    private final String accessToken = "600a2e32bd79447d9ac76d2d8e3c0122";
    //每次都要重新获取
    private final String code = "9e9c01bea5afb25e05986e7d048f1690ecb0bb1413c47a65dab3b7ab489b";

    @Autowired
    private ApiConfig apiConfig;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 获取令牌 access_token
     *
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void getAccessToken() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        String appKey = apiConfig.getAppKey();
        String appSecret = apiConfig.getAppSecret();
        Map<String, String> urlMap = apiConfig.getUrl();
        String timeStamp = String.valueOf(System.currentTimeMillis());

        Map<String, String> paramMap = new HashMap<>(16);
        paramMap.put("appKey", appKey);
        paramMap.put("timestamp", timeStamp);
        String signature = SignHelper.sign(paramMap, appSecret);
        paramMap.put("signature", signature);
        String getUrl = urlMap.get("getAccessToken") + "?appKey=" + appKey + "&timestamp=" + timeStamp + "&signature=" + signature;
        ResponseEntity<String> result = restTemplate.getForEntity(URI.create(getUrl), String.class);

        HttpStatus status = result.getStatusCode();
        HttpHeaders headers = result.getHeaders();
        String body = result.getBody();
        ApiResult apiResult = new Gson().fromJson(body, ApiResult.class);
        String code = apiResult.getCode();
        String message = apiResult.getMessage();
        Map<String, Object> data = apiResult.getData();
        String accessToken = (String) data.get("access_token");
        System.out.println(body);
        System.out.println("accessToken ==========" + accessToken);
    }

    /**
     * 通过临时code获取用户信息
     * 响应格式 {"code":"00000","message":"成功！","data":{"yhtUserId":"7f8d88ab-1bfa-4183-be47-28abbf803c0f","tenantId":"kuwm0ptf"}}
     */
    @Test
    public void getBaseInfoByCode() {
        String url = apiConfig.getUrl().get("getBaseInfoByCode");
        String getBaseInfoUrl = url + "?access_token=" + accessToken + "&code=" + code;

        String result = restTemplate.getForObject(getBaseInfoUrl, String.class);
        System.out.println(result);
        ApiResult apiResult = new Gson().fromJson(result, ApiResult.class);
        Map<String, Object> data = apiResult.getData();
        System.out.println(new Gson().toJson(data));
    }

    @Test
    public void createTodo() {
        String todoUrl = apiConfig.getUrl().get("todo");
        String url = todoUrl + "?access_token=" + accessToken;

        Map<String, Object> param = new HashMap<>();
        param.put("appId", 0);
        param.put("content", "内容测试");
        param.put("title", "测试标题");
        param.put("businessKey", UUID.randomUUID());
        param.put("typeName", "测试页签");
        param.put("yyUserIds", Collections.singletonList("7f8d88ab-1bfa-4183-be47-28abbf803c0f"));
        param.put("tenantId", "kuwm0ptf");
        param.put("mUrl", "www.baidu.com");
        param.put("webUrl", "www.baidu.com");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        System.out.println(new Gson().toJson(param));
        HttpEntity<String> entity = new HttpEntity<>(new Gson().toJson(param), headers);

        ResponseEntity<String> result = restTemplate.postForEntity(url, entity, String.class);
        System.out.println(result.getBody());
    }
}
