package com.yonyou.test.api;

import lombok.Data;

import java.util.Map;

@Data
public class ApiResult {
    private String code;
    private String message;
    private Map<String, Object> data;
}
