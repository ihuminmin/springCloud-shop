package com.yonyou.test;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "yonyou.api")
public class ApiConfig {
    private String appKey;
    private String appSecret;
    private Map<String, String> url;
}
