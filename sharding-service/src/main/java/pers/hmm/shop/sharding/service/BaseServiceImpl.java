package pers.hmm.shop.sharding.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hmm.shop.sharding.dao.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Author: hmm
 * @Date: 2021/8/18
 * @Description:
 */
@Service(value = "baseService")
public class BaseServiceImpl implements BaseService {

    @Autowired
    private BaseMapper baseMapper;

    @Override
    public void insert() {

    }

    @Override
    public void delete() {

    }

    @Override
    public List<Map<String, Object>> get() {
        return baseMapper.get();
    }

    @Override
    public void update() {

    }
}
