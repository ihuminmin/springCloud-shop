package pers.hmm.shop.sharding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pers.hmm.shop.sharding.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @Author: hmm
 * @Date: 2021/8/18
 * @Description: sharding-jdbc 增删改查
 */
@RequestMapping("base")
@RestController
public class BaseController {

    @Autowired
    private BaseService baseService;

    @GetMapping("get")
    private List<Map<String, Object>> get(@RequestParam String id) {
        List<Map<String, Object>> list = baseService.get();
        return list;
    }
}
