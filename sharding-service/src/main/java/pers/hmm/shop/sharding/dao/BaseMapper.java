package pers.hmm.shop.sharding.dao;

import org.apache.ibatis.annotations.Mapper;
import org.checkerframework.checker.signature.qual.FieldDescriptorForPrimitiveOrArrayInUnnamedPackage;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Author: hmm
 * @Date: 2021/8/19
 * @Description:
 */
@Mapper
public interface BaseMapper {
    void insert();
    void delete();
    List<Map<String, Object>> get();
    void update();
}
