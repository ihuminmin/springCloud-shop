package pers.hmm.shop.sharding.service;

import java.util.List;
import java.util.Map;

public interface BaseService {

    void insert();
    void delete();
    List<Map<String, Object>> get();
    void update();
}
