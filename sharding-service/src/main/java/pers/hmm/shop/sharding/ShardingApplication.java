package pers.hmm.shop.sharding;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * User: Administrator
 * Date: 2021/3/24
 * Time: 16:11
 * Description:
 */
@SpringCloudApplication
public class ShardingApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShardingApplication.class);
    }
}
