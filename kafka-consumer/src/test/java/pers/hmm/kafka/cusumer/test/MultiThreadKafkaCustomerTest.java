package pers.hmm.kafka.cusumer.test;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * User: Administrator
 * Date: 2021/1/28
 * Time: 10:37
 * Description:
 * 多线程消费
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MultiThreadKafkaCustomerTest {
    public static final String brokerList = "10.5.7.57:9092";
    public static final String topic = "topic-demo";
    public static final String groupId = "group.demo";
    public static final int threadNum = 8;

    public static Properties initConfig() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        return properties;
    }

    @Test
    public void consumer() {
        Properties prop = initConfig();
        KafkaConsumerThread kafkaConsumerThread = new KafkaConsumerThread(prop, topic, threadNum);
        new Thread(kafkaConsumerThread).start();
    }

    public static class KafkaConsumerThread implements Runnable {
        private KafkaConsumer<String, String> kafkaConsumer;
        private ExecutorService executorService;

        public KafkaConsumerThread(Properties props, String topic, int threadNum) {
            kafkaConsumer = new KafkaConsumer<>(props);
            kafkaConsumer.subscribe(Collections.singletonList(topic));
            executorService = new ThreadPoolExecutor(threadNum, threadNum, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(1000), new ThreadPoolExecutor.CallerRunsPolicy());
        }

        @Override
        public void run() {
            try {
                while(true) {
                    ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    if (!records.isEmpty()) {
                        executorService.submit(new RecordHandler(records));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                kafkaConsumer.close();
            }
        }
    }

    public static class RecordHandler implements Runnable {
        public final ConsumerRecords<String, String> records;

        public RecordHandler(ConsumerRecords<String, String> records) {
            this.records = records;
        }

        @Override
        public void run() {
            //消费消息业务逻辑
            for (TopicPartition tp : records.partitions()) {
                List<ConsumerRecord<String, String>> list = this.records.records(tp);
                long lastConsumerOffset = list.get(list.size() - 1).offset();
            }
        }
    }
}
