package pers.hmm.kafka.cusumer.test;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.util.*;

/**
 * User: Administrator
 * Date: 2020/12/28
 * Time: 13:02
 * Description:
 *
 * 消费者按照周期自动提交offset 默认为5秒
 * auto.commit.interval.ms = 5000
 * 当消费者找不到记录的消费位移 从哪里开始消费 默认从分区末尾
 * auto.offset.reset = latest
 * bootstrap.servers = [10.5.7.57:9092]
 * check.crcs = true
 * client.id =
 * connections.max.idle.ms = 540000
 * default.api.timeout.ms = 60000
 * 消费者自动提交位移  默认true
 * enable.auto.commit = true
 * exclude.internal.topics = true
 * 一次poll拉取的最大数据量 默认50M
 * fetch.max.bytes = 52428800
 * 拉取等待最大时间
 * fetch.max.wait.ms = 500
 * 一次poll拉取的最小数据量 默认1b
 * fetch.min.bytes = 1
 * group.id = group.demo
 * heartbeat.interval.ms = 3000
 * interceptor.classes = []
 * internal.leave.group.on.close = true
 * isolation.level = read_uncommitted
 * key.deserializer = class org.apache.kafka.common.serialization.StringDeserializer
 * 每个分区里拉取的最大数据量 默认1M
 * max.partition.fetch.bytes = 1048576
 * max.poll.interval.ms = 300000
 * max.poll.records = 500
 * metadata.max.age.ms = 300000
 * metric.reporters = []
 * metrics.num.samples = 2
 * metrics.recording.level = INFO
 * metrics.sample.window.ms = 30000
 * partition.assignment.strategy = [class org.apache.kafka.clients.consumer.RangeAssignor]
 * receive.buffer.bytes = 65536
 * reconnect.backoff.max.ms = 1000
 * reconnect.backoff.ms = 50
 * request.timeout.ms = 30000
 * retry.backoff.ms = 100
 * sasl.client.callback.handler.class = null
 * sasl.jaas.config = null
 * sasl.kerberos.kinit.cmd = /usr/bin/kinit
 * sasl.kerberos.min.time.before.relogin = 60000
 * sasl.kerberos.service.name = null
 * sasl.kerberos.ticket.renew.jitter = 0.05
 * sasl.kerberos.ticket.renew.window.factor = 0.8
 * sasl.login.callback.handler.class = null
 * sasl.login.class = null
 * sasl.login.refresh.buffer.seconds = 300
 * sasl.login.refresh.min.period.seconds = 60
 * sasl.login.refresh.window.factor = 0.8
 * sasl.login.refresh.window.jitter = 0.05
 * sasl.mechanism = GSSAPI
 * security.protocol = PLAINTEXT
 * send.buffer.bytes = 131072
 * session.timeout.ms = 10000
 * ssl.cipher.suites = null
 * ssl.enabled.protocols = [TLSv1.2, TLSv1.1, TLSv1]
 * ssl.endpoint.identification.algorithm = https
 * ssl.key.password = null
 * ssl.keymanager.algorithm = SunX509
 * ssl.keystore.location = null
 * ssl.keystore.password = null
 * ssl.keystore.type = JKS
 * ssl.protocol = TLS
 * ssl.provider = null
 * ssl.secure.random.implementation = null
 * ssl.trustmanager.algorithm = PKIX
 * ssl.truststore.location = null
 * ssl.truststore.password = null
 * ssl.truststore.type = JKS
 * value.deserializer = class org.apache.kafka.common.serialization.StringDeserializer
 * <p>
 * 在旧消费者客户端，消息的位移是保存在zookeeper中，新的消费者客户端，消费位移保存在kafka的内部
 * 主题_consumer_offsets中，消费者在消费完消息后执行位移的提交。
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class KafkaCustomerTest {
    private static final String brokerList = "10.5.7.57:9092";
    private static final String topic = "topic-demo";
    private static final String groupId = "group.demo";

    @Test
    public void consumer() {
        Properties properties = new Properties();
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("bootstrap.servers", brokerList);
        properties.put("group.id", groupId);

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        //订阅主题
        consumer.subscribe(Collections.singletonList(topic));
        //使用正则表达式
        //consumer.subscribe(Pattern.compile("topic-, *"));

        //查询主题的元数据信息
        List<PartitionInfo> partitionInfos = consumer.partitionsFor(topic);
        for (PartitionInfo info : partitionInfos) {
            System.out.println(info.toString());
        }

        Set<TopicPartition> assignment = new HashSet<>();
        while(assignment.size() > 0){
            consumer.poll(Duration.ofMillis(100));
            //获取消费者所分配到的分区信息
            assignment = consumer.assignment();
        }

        for (TopicPartition partition : assignment) {
            // poll方法 无法精确掌控消费的起始位置
            // seek方法 可以让我们追前消费或回溯消费 重置所消费分区的偏移量
            consumer.seek(partition, 10);
        }

        while (true) {
            //消费有两种方式
            //推：服务端将消息推送给消费者
            //拉：消费者主动向服务端发起请求拉取消息

            // x-5 x-4 x-3 x-2 x-1 x x+1 x+2 x+3 x+4 x+5
            // 消费者消费消息 默认是每5秒自动提交
            // 拉取一次消息 如果消费异常出现在提交前 会出现重复消费；如果位移提交后，消息消费出现异常 会出现消息丢失
            // 如何解决重复消费和消息丢失？ 手动提交
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));

            // 获取消息处理业务逻辑
            for (ConsumerRecord<String, String> record : records) {
                System.out.println(record.value());
                // 消费者消费消息 lastConsumedOffset、committedOffset、position的理解：
                // lastConsumedOffset: 已经提交的最后一个消息的offset
                // committedOffset: 消费完消息之后提交的offset
                // position：下一次要拉取得第一个消息得offset

                TopicPartition topicPartition = new TopicPartition(topic, record.partition());
                List<ConsumerRecord<String, String>> list = records.records(topicPartition);
                long lastConsumedOffset = list.get(list.size() - 1).offset();
                System.out.println("lastConsumedOffset: =====" + lastConsumedOffset);
                long position = consumer.position(topicPartition);
                System.out.println("position: =====" + position);
                OffsetAndMetadata committed = consumer.committed(topicPartition);
                System.out.println("committedOffset: =====" + committed.offset());
            }

            // 手动提交 enable.auto.commit配置为false
            // 1.手动同步提交
            consumer.commitSync();
            // 2.手动异步提交
            consumer.commitAsync();
        }
    }
}
