package pers.hmm.kafka.cusumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * User: Administrator
 * Date: 2020/12/15
 * Time: 13:25
 * Description:
 */
@SpringBootApplication
public class KafkaCusumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaCusumerApplication.class);
    }
}
