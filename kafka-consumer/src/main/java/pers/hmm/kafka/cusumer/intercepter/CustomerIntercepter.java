package pers.hmm.kafka.cusumer.intercepter;

import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;

/**
 * User: Administrator
 * Date: 2021/1/28
 * Time: 10:01
 * Description:
 * 消费者拦截器
 */
public class CustomerIntercepter implements ConsumerInterceptor {
    /**
     *   kafkaConsumer会在poll()方法之前调用拦截器的onConsume()方法对消息进行定制化操作
     *  比如修改返回的消息内容、按照某种规则过滤消息，如果onConsume()中抛出异常，会被记录到
     *  日志中，不会再向上抛。
     * @param records
     * @return
     */
    @Override
    public ConsumerRecords onConsume(ConsumerRecords records) {
        return null;
    }

    /**
     * kafkaConsumer会在提交完消息位移后调用onCommit方法，可以使用这个方法记录跟踪
     * 所提交的位移信息
     * @param offsets
     */
    @Override
    public void onCommit(Map offsets) {

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
