package pers.hmm.test.kafka;

import org.apache.kafka.clients.producer.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import per.hmm.kakfa.producer.ProducerInterceptPrefix;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * User: Administrator
 * Date: 2020/12/15
 * Time: 11:35
 * Description:
 * 默认1，只要分区leader副本成功写入就返回；0不等待服务端响应；-1/all:所有副本都写入成功
 * acks = 1
 * BufferPool对特定大小的ByteBuffer进行管理 这个大小由batch.size配置
 * batch.size = 16384
 * bootstrap.servers = [10.5.7.57:9092]
 * 消息收集器批量发送大小  默认32M
 * buffer.memory = 33554432
 * client.id =
 * 消息的压缩方式 默认为none
 * compression.type = none
 * 多久之后关闭闲置的连接 默认9分钟
 * connections.max.idle.ms = 540000
 * enable.idempotence = false
 * interceptor.classes = []
 * key.serializer = class org.apache.kafka.common.serialization.StringSerializer
 * 生产者发送producerBatch之前等待更多的producerRecord加入的时间 默认为0
 * linger.ms = 0
 * 发送的最大阻塞时间
 * max.block.ms = 60000
 * 发送后未获取响应的请求保存到InFlightRequest 此配置表示每个连接最多缓存的请求数
 * max.in.flight.requests.per.connection = 5
*  生产者客户端发送消息的最大值 默认1M
 * max.request.size = 1048576
 * metadata.max.age.ms = 300000
 * metric.reporters = []
 * metrics.num.samples = 2
 * metrics.recording.level = INFO
 * metrics.sample.window.ms = 30000
 * partitioner.class = class org.apache.kafka.clients.producer.internals.DefaultPartitioner
 * 设置socket接受消息缓冲区的大小 默认为32kb，如果为-1则使用操作系统默认值
 * receive.buffer.bytes = 32768
 * reconnect.backoff.max.ms = 1000
 * reconnect.backoff.ms = 50
 * 生产者等待消息响应的超时时间 需要比broker端参数replica.lag.time.max.ms的值要大
 * request.timeout.ms = 30000
 * 配置生产者重试次数 默认0
 * retries = 0
 * 设定两次重试之间的间隔 默认100
 * retry.backoff.ms = 100
 * sasl.client.callback.handler.class = null
 * sasl.jaas.config = null
 * sasl.kerberos.kinit.cmd = /usr/bin/kinit
 * sasl.kerberos.min.time.before.relogin = 60000
 * sasl.kerberos.service.name = null
 * sasl.kerberos.ticket.renew.jitter = 0.05
 * sasl.kerberos.ticket.renew.window.factor = 0.8
 * sasl.login.callback.handler.class = null
 * sasl.login.class = null
 * sasl.login.refresh.buffer.seconds = 300
 * sasl.login.refresh.min.period.seconds = 60
 * sasl.login.refresh.window.factor = 0.8
 * sasl.login.refresh.window.jitter = 0.05
 * sasl.mechanism = GSSAPI
 * security.protocol = PLAINTEXT
 * 设置socket发送消息缓冲区的大小，默认为128kb
 * send.buffer.bytes = 131072
 * ssl.cipher.suites = null
 * ssl.enabled.protocols = [TLSv1.2, TLSv1.1, TLSv1]
 * ssl.endpoint.identification.algorithm = https
 * ssl.key.password = null
 * ssl.keymanager.algorithm = SunX509
 * ssl.keystore.location = null
 * ssl.keystore.password = null
 * ssl.keystore.type = JKS
 * ssl.protocol = TLS
 * ssl.provider = null
 * ssl.secure.random.implementation = null
 * ssl.trustmanager.algorithm = PKIX
 * ssl.truststore.location = null
 * ssl.truststore.password = null
 * ssl.truststore.type = JKS
 * transaction.timeout.ms = 60000
 * transactional.id = null
 * value.serializer = class org.apache.kafka.common.serialization.StringSerializer
 * <p>
 *
 *      整个生产者客户端由两个线程协调运行，这两个线程分别是主线程和sender线程，主线程中producer创建消息，
 * 经由拦截器、序列化器和分区器后，缓存到消息收集器RecordAccumulator中。sender线程负责从消息收集器
 * 中获取消息，并发送到kafka中。
 *
 *
 */
@SpringBootTest(classes = KafkaProducerTest.class)
@RunWith(SpringRunner.class)
public class KafkaProducerTest {

    private static final String brokerList = "10.5.7.57:9092";
    private static final String topic = "topic-demo";

    @Test
    public void produce() throws ExecutionException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        properties.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, 100);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList);
        properties.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, ProducerInterceptPrefix.class.getName());
        //创建kafka生产者实例 kafkaProducer是线程安全的 可以多线程共享
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        //封装消息
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, "你好");
        //发送消息 配置retries_config参数后 发生可重试异常会重试

//        发后即忘 不关心发送结果 性能最高 可靠性最差
//        kafkaProducer.send(record);

        //阻塞获取发送结果 同步方式 可判断返回结果 捕获异常 做进一步处理
//        RecordMetadata metadata = kafkaProducer.send(record).get();

        //异步方式 设置callback回调
        //发送成功：metadata不为null exception为null；发送失败：metadata为null exception不为null
        //回调函数的调用 可以保证分区有序
        kafkaProducer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                if (exception == null) {
                    //发送成功做处理
                    System.out.println("异步发送回调打印：发送 成功 >>>>>>>>>>>>>>>>>>");
                } else {
                    //发送失败
                    System.out.println("异步发送回调打印：发送 失败 >>>>>>>>>>>>>>>>>>");
                }
            }
        });

        //阻塞等待之前所有的消息发送请求完成后 关闭
        kafkaProducer.close();
    }
}
