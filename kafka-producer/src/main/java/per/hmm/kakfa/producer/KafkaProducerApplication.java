package per.hmm.kakfa.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * User: Administrator
 * Date: 2020/12/15
 * Time: 13:19
 * Description:
 */
@SpringBootApplication
public class KafkaProducerApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class);
    }
}
