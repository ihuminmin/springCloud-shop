package pers.hmm.shop.common.enums;

/**
 * @author 胡敏敏
 * Date 2019/7/5
 * DESC:
 */
public enum ExceptionEnums {
    //
    FILE_TYPE_NOT_ALLOW(500, "上传类型不允许"),
    //
    UPLOAD_IMAGE_FAILED(500, "上传文件失败"),
    //
    POLICY_CANNOT_BE_NULL(400, "价格不能为空!"),
    //
    CATEGORY_NOT_FOUND(404, "未找到商品目录"),
    //
    BRAND_ADD_ERROR(500, "添加品牌失败！"),
    //
    Brand_NOT_FOUND(404, "未找到商品品牌"),
    //
    SERVICE_UNAVAILABLE(503, "服务暂时不可用");

    private int code;
    private String msg;

    ExceptionEnums() {
    }

    ExceptionEnums(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
