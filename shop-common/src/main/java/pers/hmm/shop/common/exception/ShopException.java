package pers.hmm.shop.common.exception;

import pers.hmm.shop.common.enums.ExceptionEnums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 * @author admin
 */

public class ShopException extends RuntimeException {
    private ExceptionEnums exceptionEnums;

    public ShopException(ExceptionEnums exceptionEnums) {
        this.exceptionEnums = exceptionEnums;
    }


    public ExceptionEnums getExceptionEnums() {
        return exceptionEnums;
    }

    public void setExceptionEnums(ExceptionEnums exceptionEnums) {
        this.exceptionEnums = exceptionEnums;
    }
}
