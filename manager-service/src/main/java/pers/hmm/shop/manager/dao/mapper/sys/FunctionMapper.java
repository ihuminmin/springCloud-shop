package pers.hmm.shop.manager.dao.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import pers.hmm.shop.manager.dao.entity.Function;

import java.util.List;

@Mapper
public interface FunctionMapper {
    List<Function> getFunctions();

    List<Function> getLevelFunctions(Long funcParentId);

    void addFunction(Function function);

    void delFunction(Long functionId);

    void editFunction(Function function);
}
