package pers.hmm.shop.manager.controller.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import pers.hmm.shop.manager.dao.entity.Function;
import pers.hmm.shop.manager.service.FunctionService;
import pers.hmm.shop.manager.support.SuccessResult;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("sys/functions")
public class FunctionController {
    private Logger logger = LoggerFactory.getLogger(FunctionController.class);

    @Autowired
    private FunctionService functionService;

    /**
     * 查询所有系统功能
     * @return
     */
    @GetMapping("getAll")
    private SuccessResult<List<Function>> getFunctions() {
        List<Function> list = functionService.getFunctions();
        return new SuccessResult<>(list);
    }

    /**
     * 不同层级功能
     * @param functionParentId
     * @return
     */
    @GetMapping("getLevelFunctions")
    private SuccessResult<List<Function>> getLevelFunctions(@RequestParam Long functionParentId) {
        List<Function> list = functionService.getLevelFunctions(functionParentId);
        return new SuccessResult<>(list);
    }

    /**
     * 新增功能
     * @param function
     * @return
     */
    @PostMapping("addFunction")
    private SuccessResult<String> addFunction(@RequestBody Function function) {
        functionService.addFunction(function);
        return new SuccessResult<>(null);
    }

    @PostMapping("delFunction")
    private SuccessResult<String> addFunction(@RequestParam Long functionId) {
        functionService.delFunction(functionId);
        return new SuccessResult<>(null);
    }

    @PostMapping("editFunction")
    private SuccessResult<String> editFunction(@RequestBody Function function) {
        functionService.editFunction(function);
        return new SuccessResult<>(null);
    }

}
