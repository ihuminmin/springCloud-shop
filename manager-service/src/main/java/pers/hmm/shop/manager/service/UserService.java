package pers.hmm.shop.manager.service;

import pers.hmm.shop.manager.dao.entity.UserInfo;

import java.util.List;

public interface UserService {
    UserInfo findByUsername(String username);

    List<UserInfo> getUserList();

    void createUser(UserInfo userInfo);

    void deleteUser(Long userId);
}
