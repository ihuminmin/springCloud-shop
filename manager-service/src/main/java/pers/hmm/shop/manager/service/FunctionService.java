package pers.hmm.shop.manager.service;

import pers.hmm.shop.manager.dao.entity.Function;

import java.util.List;

public interface FunctionService {

    List<Function> getFunctions();

    List<Function> getLevelFunctions(Long funcParentId);

    void addFunction(Function function);

    void delFunction(Long functionId);

    void editFunction(Function function);
}
