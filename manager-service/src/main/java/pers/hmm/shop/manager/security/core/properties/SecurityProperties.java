/**
 * 
 */
package pers.hmm.shop.manager.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author humm
 *
 */
@ConfigurationProperties(prefix = "manage.security")
public class SecurityProperties {
	
	private BrowserProperties browser = new BrowserProperties();
	
	private ValidateCodeProperties code = new ValidateCodeProperties();

	private GiteeProperties gitee = new GiteeProperties();
	
	public BrowserProperties getBrowser() {
		return browser;
	}

	public void setBrowser(BrowserProperties browser) {
		this.browser = browser;
	}

	public ValidateCodeProperties getCode() {
		return code;
	}

	public void setCode(ValidateCodeProperties code) {
		this.code = code;
	}

	public GiteeProperties getGitee() {
		return gitee;
	}

	public void setGitee(GiteeProperties gitee) {
		this.gitee = gitee;
	}
}

