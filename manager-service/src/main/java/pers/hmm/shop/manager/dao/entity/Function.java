package pers.hmm.shop.manager.dao.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 功能（菜单、操作）
 */
public class Function implements Serializable {
    private static final long serialVersionUID = -4989830320728129851L;

    private Long functionId;
    private Long funcParentId;
    private String funcCode;
    private String funcName;
    // 0菜单 1按钮
    private String funcType;
    // 路径
    private String url;
    // 状态
    private String open;
    // 图标
    private String icon;
    private String remark;
    private String creator;
    private Date createTime;
    private String updator;
    private Date updateTime;

    private List<Function> children;
    // 有权限的角色
    private List<String> roles;

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Long getFuncParentId() {
        return funcParentId;
    }

    public void setFuncParentId(Long funcParentId) {
        this.funcParentId = funcParentId;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getFuncType() {
        return funcType;
    }

    public void setFuncType(String funcType) {
        this.funcType = funcType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<Function> getChildren() {
        return children;
    }

    public void setChildren(List<Function> children) {
        this.children = children;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
