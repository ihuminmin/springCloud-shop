package pers.hmm.shop.manager.service.impl;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pers.hmm.shop.manager.dao.entity.UserInfo;
import pers.hmm.shop.manager.dao.mapper.sys.UserMapper;
import pers.hmm.shop.manager.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserInfo findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public List<UserInfo> getUserList() {
        PageHelper.startPage(1, 5);
        return userMapper.getUserList();
    }

    @Override
    public void createUser(UserInfo userInfo) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String principal = (String) authentication.getPrincipal();
        userInfo.setCreator(principal);
        userMapper.createUser(userInfo);
    }

    @Override
    public void deleteUser(Long userId) {
        userMapper.deleteUser(userId);
    }
}
