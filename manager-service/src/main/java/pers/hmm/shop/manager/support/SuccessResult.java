package pers.hmm.shop.manager.support;

/**
 * 封装响应数据
 */
public class SuccessResult<T> extends AbstractResult<T> {

    private static final long serialVersionUID = -650775647720070799L;

    public SuccessResult(T data) {
       super(200, "success", data);
    }
}
