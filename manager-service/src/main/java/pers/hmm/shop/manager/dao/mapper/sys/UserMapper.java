package pers.hmm.shop.manager.dao.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import pers.hmm.shop.manager.dao.entity.UserInfo;

import java.util.List;

@Mapper
public interface UserMapper {
    UserInfo findByUsername(String username);

    List<UserInfo> getUserList();

    void createUser(UserInfo userInfo);

    void deleteUser(Long userId);
}
