package pers.hmm.shop.manager.support;

public class FailureResult<T> extends AbstractResult<T> {

    private static final long serialVersionUID = -4656356137168043230L;

    public FailureResult(String message, T data) {
        super(-1, message, data);
    }
}
