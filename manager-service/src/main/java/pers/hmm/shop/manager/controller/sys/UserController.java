package pers.hmm.shop.manager.controller.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pers.hmm.shop.manager.dao.entity.Function;
import pers.hmm.shop.manager.dao.entity.UserInfo;
import pers.hmm.shop.manager.service.FunctionService;
import pers.hmm.shop.manager.service.UserService;
import pers.hmm.shop.manager.support.SuccessResult;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController implements EnvironmentAware {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/info")
    private SuccessResult<UserInfo> getUserInfo() {
        logger.info("接收到请求！");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String principal = (String) authentication.getPrincipal();
        UserInfo userInfo = userService.findByUsername(principal);
        return new SuccessResult<>(userInfo);
    }

    @PostMapping("/logout")
    private SuccessResult<Object> logOut(RedirectAttributes redirectAttributes) {
        ((FlashMap) ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest().getAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE)).put("name", "张三丰");
        redirectAttributes.addFlashAttribute("orderId", "XXX");
        redirectAttributes.addAttribute("local", "zh-cn");
        return new SuccessResult<>(null);
    }

    @PostMapping("/addUser")
    private SuccessResult<String> createUser(@RequestBody UserInfo userInfo) {
        System.out.println(userInfo.getUsername());
        userService.createUser(userInfo);
        return new SuccessResult<>(null);
    }

    @PostMapping("/getUserList")
    private SuccessResult<List<UserInfo>> getUserList() {
        List<UserInfo> list = userService.getUserList();
        return new SuccessResult<>(list);
    }

    @PostMapping("/deleteUser")
    private SuccessResult<String> deleteUser(@RequestParam Long userId) {
        userService.deleteUser(userId);
        return new SuccessResult<>(null);
    }

    private Environment environment = null;


    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
