package pers.hmm.shop.manager.dao.mapper.sys;

import org.apache.ibatis.annotations.Mapper;
import pers.hmm.shop.manager.dao.entity.Permission;

import java.util.List;

@Mapper
public interface PermissonMapper {
    List<Permission> findAll();
}
