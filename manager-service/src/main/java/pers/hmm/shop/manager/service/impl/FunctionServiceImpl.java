package pers.hmm.shop.manager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pers.hmm.shop.manager.dao.entity.Function;
import pers.hmm.shop.manager.dao.mapper.sys.FunctionMapper;
import pers.hmm.shop.manager.service.FunctionService;

import java.util.List;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private FunctionMapper functionMapper;

    @Override
    public List<Function> getFunctions() {
        return functionMapper.getFunctions();
    }

    @Override
    public List<Function> getLevelFunctions(Long funcParentId) {
        return functionMapper.getLevelFunctions(funcParentId);
    }

    @Override
    public void addFunction(Function function) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String principal = (String) authentication.getPrincipal();
        function.setCreator(principal);
        function.setUpdator(principal);
        functionMapper.addFunction(function);
    }

    @Override
    public void delFunction(Long functionId) {
        functionMapper.delFunction(functionId);
    }

    @Override
    public void editFunction(Function function) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String principal = (String) authentication.getPrincipal();
        function.setUpdator(principal);
        functionMapper.editFunction(function);
    }
}
