package pers.hmm.shop.manager.security.core.properties;

/**
 * @author： hmm
 * @date： 2020/6/22 14:18
 * @description： TODO
 * @modifiedBy：
 */
public class GiteeProperties {

    private String clientid;
    private String clientSecret;
    private String callback;

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }
}
