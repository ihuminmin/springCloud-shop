package pers.hmm.shop.manager.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pers.hmm.shop.common.utils.JsonUtils;
import pers.hmm.shop.manager.security.core.properties.SecurityProperties;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author： hmm
 * @date： 2020/6/22 14:23
 * @description： TODO
 * @modifiedBy：
 */
@RestController
public class AuthController {
    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/auth/gitee")
    private void giteeAuth(HttpServletResponse response) throws IOException {
        // Step1：获取Authorization Code
        String url = "https://gitee.com/oauth/authorize?response_type=code" +
                "&client_id=" + securityProperties.getGitee().getClientid() +
                "&redirect_uri=" + securityProperties.getGitee().getCallback() +
                "&response_type=code" +
                "&scope=user_info";
        logger.info(url);
        response.sendRedirect(url);
    }

    @GetMapping("/auth/gitee/callback")
    private String callBack(@RequestParam(name = "code") String code) {
        logger.info(code);
        String tokenUrl = "https://gitee.com/oauth/token"
                + "?grant_type=authorization_code&code=" + code
                + "&client_id=" + securityProperties.getGitee().getClientid()
                + "&redirect_uri=" + securityProperties.getGitee().getCallback()
                + "&client_secret=" + securityProperties.getGitee().getClientSecret();
        logger.info(tokenUrl);
        HttpEntity<String> httpEntity = httpEntity();

        ResponseEntity<String> response = restTemplate.postForEntity(tokenUrl, httpEntity, String.class);
        logger.info(response.toString());
        GiteeAccessToken giteeAccessToken = JsonUtils.jsonToBean(response.getBody(), GiteeAccessToken.class);

        String userUrl = "https://gitee.com/api/v5/user?access_token=" + giteeAccessToken.getAccessToken();
        ResponseEntity<String> result = restTemplate.exchange(userUrl, HttpMethod.GET, httpEntity, String.class);
        String userInfo = result.getBody();
        logger.info(userInfo);
        return userInfo;
    }

    private HttpEntity<String> httpEntity() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        requestHeaders.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
        requestHeaders.add(HttpHeaders.ACCEPT, "*/*");
        return new HttpEntity<>(null, requestHeaders);
    }
}
